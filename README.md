# Gemfile Parser

Gemfile Parser is a go package aimed at parsing Ruby `Gemfile` files.
It uses [tree-sitter](https://tree-sitter.github.io/tree-sitter/using-parsers)
to create an abstract syntax tree that is walked to detect the following.

1. The specificied Ruby version.
1. The specified source.
1. Gems with their groups, source, and version specifiers.

## ⚠️DISCLAIMER⚠️

This project is still in a very early pre-release stage. Therefore, you
should expect bugs and a lot of edge cases that have not yet been implemented.
