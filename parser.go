package gemfileparser

import (
	"context"
	"fmt"
	"io"

	sitter "github.com/smacker/go-tree-sitter"
	"github.com/smacker/go-tree-sitter/ruby"
)

type Gemfile struct {
	Source string `json:"source,omitempty"`
	Ruby   string `json:"ruby,omitempty"`
	Gems   []Gem  `json:"gems,omitempty"`
}

type Gem struct {
	Name      string
	Specifier string
	Require   bool
	Groups    []string
	Source    string
}

func Parse(ctx context.Context, r io.Reader) (*Gemfile, error) {
	parser := sitter.NewParser()
	parser.SetLanguage(ruby.GetLanguage())

	code, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}

	tree, err := parser.ParseCtx(ctx, nil, code)
	if err != nil {
		return nil, err
	}

	var gemfile Gemfile
	seen := make(map[*sitter.Node]bool)
	visitFunc := func(call *sitter.Node) error {
		// Only look for function calls
		if call.Type() != "call" {
			return nil
		}

		if seen[call] {
			return nil
		}
		seen[call] = true

		method := call.ChildByFieldName("method")
		args := call.ChildByFieldName("arguments")

		switch method.Content(code) {
		case "source":
			if block := call.ChildByFieldName("block"); block != nil {
				source := args.NamedChild(0).NamedChild(0).Content(code)
				for i := 0; uint32(i) < block.NamedChildCount(); i++ {
					subcall := block.NamedChild(i)
					gem := getGem(subcall, code)
					gem.Groups = []string{"default"}
					gem.Source = source
					gemfile.Gems = append(gemfile.Gems, gem)
					seen[subcall] = true
				}
			} else {
				gemfile.Source = args.Child(0).Child(1).Content(code)
			}
		case "ruby":
			gemfile.Ruby = args.NamedChild(0).NamedChild(0).Content(code)
		case "gem":
			gem := getGem(call, code)
			gem.Groups = []string{"default"}
			gem.Source = gemfile.Source
			gemfile.Gems = append(gemfile.Gems, gem)
		case "group":
			groups := getGroups(call, code)
			block := call.ChildByFieldName("block")
			for i := 0; uint32(i) < block.NamedChildCount(); i++ {
				subcall := block.NamedChild(i)
				gem := getGem(subcall, code)
				gem.Groups = groups
				gem.Source = gemfile.Source
				gemfile.Gems = append(gemfile.Gems, gem)
				seen[subcall] = true
			}
		default:
			fmt.Printf("method %v not handled\n", method)
		}

		return nil
	}

	itr := sitter.NewIterator(tree.RootNode(), sitter.DFSMode)
	if err := itr.ForEach(visitFunc); err != nil && err != io.EOF {
		return nil, fmt.Errorf("walking gemfile ast tree: %v", err)
	}
	return &gemfile, nil
}

func getGem(n *sitter.Node, content []byte) Gem {
	var gem Gem
	if n == nil {
		return gem
	}

	args := n.ChildByFieldName("arguments")
	if args == nil {
		return gem
	}

	// First arg is required and will always have a string with string content.
	// Therefore you'll have to descend to the first child's child that holds
	// the gem name.
	gem.Name = args.NamedChild(0).NamedChild(0).Content(content)

	// All other arguments are not required so you have to check if the arg is
	// a "pair" (a named arg) or if it's a string which would be the specifier.
	for idx := 1; uint32(idx) < args.NamedChildCount(); idx++ {
		arg := args.NamedChild(idx)
		switch arg.Type() {
		case "string":
			gem.Specifier = arg.NamedChild(0).Content(content)
		case "pair":
			key := arg.ChildByFieldName("key").Content(content)
			if key == "require" {
				value := arg.ChildByFieldName("value").Content(content)
				gem.Require = (value == "true")
			}
			if key == "group" {
				gem.Groups = getInlineGroups(arg.ChildByFieldName("value"), content)
			}
		}
	}
	// Parsing is done so we can save the spec.
	return gem
}

func getGroups(call *sitter.Node, content []byte) []string {
	if call == nil {
		return nil
	}

	var groups []string
	args := call.ChildByFieldName("arguments")
	for i := 0; uint32(i) < args.NamedChildCount(); i++ {
		groupSymbol := args.NamedChild(i)
		group := symbolToString(groupSymbol, content)
		groups = append(groups, group)
	}

	return groups
}

func getInlineGroups(n *sitter.Node, content []byte) []string {
	groupArgNode := n.ChildByFieldName("value")
	switch groupArgNode.Type() {
	case "simple_symbol":
		symbolNodeContent := groupArgNode.Content(content)
		group := symbolNodeContent[1:]
		return []string{group}
	case "argument_list":
		var groups []string
		for i := 0; uint32(i) < groupArgNode.NamedChildCount(); i++ {
			symbolNode := groupArgNode.NamedChild(i)
			group := symbolToString(symbolNode, content)
			groups = append(groups, group)
		}
		return groups
	default:
		return nil
	}
}

func symbolToString(n *sitter.Node, content []byte) string {
	if len(n.Content(content)) == 0 {
		return ""
	}

	return n.Content(content)[1:]
}
