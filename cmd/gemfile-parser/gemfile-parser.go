package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"log"
	"os"
	"path/filepath"

	parser "gitlab.com/oscar.alberto.tovar/gemfile-parser"
)

var (
	flagInputFile   = flag.String("file", "Gemfile", "path to the target Gemfile")
	flagPrettyPrint = flag.Bool("pretty", false, "pretty prints json output")
)

func main() {
	flag.Parse()

	gemfilePath, err := filepath.Abs(*flagInputFile)
	if err != nil {
		log.Fatalf("%v", err)
	}

	log.Printf("Scanning %q\n", gemfilePath)
	ctx := context.Background()
	gemfile, err := os.Open(gemfilePath)
	if err != nil {
		log.Fatalf("Failed to open %q: %v", gemfilePath, err)
	}
	gems, err := parser.Parse(ctx, gemfile)
	if err != nil {
		log.Fatalf("Failed to parse %q: %v", gemfilePath, err)
	}
	out, err := json.Marshal(&gems)
	if err != nil {
		log.Fatalf("Producing JSON output: %v", err)
	}

	log.Println("Gemfile parsed successfully")
	if *flagPrettyPrint {
		var buf bytes.Buffer
		err = json.Indent(&buf, out, "", "    ")
		if err != nil {
			log.Fatalf("Prettifying JSON: %v", err)
		}
		log.Printf("Output:\n%v\n", buf.String())
	} else {
		log.Printf("Output: %v\n", string(out))
	}
}
