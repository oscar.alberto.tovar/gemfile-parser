# CHANGELOG

## v0.1.0

- Parse specified ruby version.
- Parse specified default gem source.
- Parse gems within a custom source.
- Parse gems within custom groups.
