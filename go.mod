module gitlab.com/oscar.alberto.tovar/gemfile-parser

go 1.18

require (
	github.com/smacker/go-tree-sitter v0.0.0-20220628134258-ac06e95cfa11
	github.com/stretchr/testify v1.8.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
