package gemfileparser

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParse(t *testing.T) {
	expect := &Gemfile{
		Source: "https://rubygems.org",
		Ruby:   "2.4.5",
		Gems: []Gem{
			{
				Name:      "puma",
				Specifier: "~> 3.12",
				Require:   false,
				Groups:    []string{"default"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "pg",
				Specifier: "",
				Require:   false,
				Groups:    []string{"default"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "sidekiq",
				Specifier: "~> 4.0",
				Require:   false,
				Groups:    []string{"default"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "sinatra",
				Specifier: "",
				Require:   false,
				Groups:    []string{"default"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "slim",
				Specifier: "",
				Require:   false,
				Groups:    []string{"default"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "nokogiri",
				Specifier: "1.6.7.2",
				Require:   false,
				Groups:    []string{"default"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "byebug",
				Specifier: "",
				Require:   false,
				Groups:    []string{"development", "test"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "spring",
				Specifier: "",
				Require:   false,
				Groups:    []string{"development"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "guard-rspec",
				Specifier: "",
				Require:   false,
				Groups:    []string{"development"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "faker",
				Specifier: "~> 1.6.1",
				Require:   false,
				Groups:    []string{"test"},
				Source:    "https://rubygems.org",
			},
			{
				Name:      "rspec",
				Specifier: "",
				Require:   false,
				Groups:    []string{"test"},
				Source:    "https://rubygems.org",
			},
		},
	}
	ctx := context.Background()
	gemfile, err := os.Open("testdata/Gemfile")
	require.NoError(t, err)
	got, err := Parse(ctx, gemfile)
	require.NoError(t, err)
	require.NotEmpty(t, got)
	require.Equal(t, expect, got)
}
